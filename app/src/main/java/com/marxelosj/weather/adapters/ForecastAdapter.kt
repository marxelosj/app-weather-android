package com.marxelosj.weather.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.marxelosj.weather.R
import com.marxelosj.weather.retrofit.Daily
import com.marxelosj.weather.utils.Constants
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ForecastAdapter(private val mForecasts: ArrayList<Daily>) :
    RecyclerView.Adapter<ForecastAdapter.DailyViewHolder>() {
    class DailyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dayOfWeek: TextView = itemView.findViewById(R.id.day_of_week)
        var dayAndMonth: TextView = itemView.findViewById(R.id.day_and_month)
        var weatherDescription: TextView = itemView.findViewById(R.id.weather_description)
        var minTemp: TextView = itemView.findViewById(R.id.min_temp)
        var maxTemp: TextView = itemView.findViewById(R.id.max_temp)
        var weatherIcon: ImageView = itemView.findViewById(R.id.weather_icon)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DailyViewHolder {
        return DailyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.forecast_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: DailyViewHolder, position: Int) {
        val forecast = mForecasts.get(position)
        val weather = forecast.weather.first()
        val temp = forecast.temp
        holder.dayOfWeek.setText(getSimpleDateFormat("EEEE", forecast.dt))
        holder.dayAndMonth.setText(getSimpleDateFormat("dd/MM", forecast.dt))
        holder.minTemp.setText(String.format(Locale.getDefault(), "%.0f°", temp?.min))
        holder.maxTemp.setText(String.format(Locale.getDefault(), "%.0f°", temp?.max))
        holder.weatherDescription.setText(weather.description)
        Glide.with(holder.itemView.context).load(weather.icon?.let { Constants.getUrlIcon(it) }).into(holder.weatherIcon)
    }

    private fun getSimpleDateFormat(format: String, timestamp: Long): String {
        return SimpleDateFormat(format, Locale.getDefault()).format(timestamp * 1000)
    }

    override fun getItemCount() = mForecasts.size

    fun addForecasts(forecasts: ArrayList<Daily>) {
        this.mForecasts.apply {
            clear()
            addAll(forecasts)
        }

    }
}