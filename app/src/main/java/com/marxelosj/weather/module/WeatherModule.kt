package com.marxelosj.weather.retrofit

import com.marxelosj.weather.repository.WeatherRepository
import com.marxelosj.weather.viewmodels.WeatherViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val weatherModule = module {

    single { WeatherRepository(get()) }

    single { createWebService() }

    viewModel { WeatherViewModel(get()) }

}

fun createWebService(): WeatherApi {
    val retrofit = WeatherService.retrofitInstance
    return retrofit!!.create(WeatherApi::class.java)
}