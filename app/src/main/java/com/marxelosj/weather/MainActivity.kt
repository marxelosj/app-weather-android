package com.marxelosj.weather

import android.Manifest
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.marxelosj.weather.adapters.ForecastAdapter
import com.marxelosj.weather.retrofit.WeatherResponse
import com.marxelosj.weather.utils.Constants
import com.marxelosj.weather.viewmodels.WeatherViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinApiExtension
import java.text.SimpleDateFormat
import java.util.*

@KoinApiExtension
class MainActivity : AppCompatActivity() {
    private val TAG: String = javaClass.simpleName

    // VIEWMODEL
    private val mWeatherViewModel: WeatherViewModel by viewModel()

    // ADAPTER
    private lateinit var mForecastAdapter: ForecastAdapter

    // UI
    private lateinit var mTodayMaterialCard: CardView
    private lateinit var mProgress: ProgressBar
    private lateinit var mCurrentTemp: TextView
    private lateinit var mCurrentWeatherDescription: TextView
    private lateinit var mCurrentHumidity: TextView
    private lateinit var mCurrentWind: TextView
    private lateinit var mCurrentSunrise: TextView
    private lateinit var mCurrentSunset: TextView
    private lateinit var mCurrentWeatherIcon: ImageView
    private lateinit var mRecycler: RecyclerView

    // LOCATION
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mCurrentLocation: Location? = null

    // DUMMY
    val locations = arrayOf("Current Location", "Madrid", "New York", "Berlín", "Praga", "Moscú")
    var locationSelected: Int = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        initInstances()
        initUI()
        initObservers()
        initLocationProviderClient()
    }

    private fun initInstances() {
        mForecastAdapter = ForecastAdapter(arrayListOf())
    }

    private fun initUI() {
        mTodayMaterialCard = findViewById(R.id.todayMaterialCard)
        mProgress = findViewById(R.id.progress)
        mCurrentTemp = findViewById(R.id.current_temp)
        mCurrentWeatherDescription = findViewById(R.id.current_weather_description)
        mCurrentHumidity = findViewById(R.id.current_humidity)
        mCurrentWind = findViewById(R.id.current_wind)
        mCurrentSunrise = findViewById(R.id.current_sunrise)
        mCurrentSunset = findViewById(R.id.current_sunset)
        mCurrentWeatherIcon = findViewById(R.id.current_weather_icon)
        mRecycler = findViewById<RecyclerView>(R.id.recycler_forecast).apply {
            setHasFixedSize(true)
            itemAnimator = DefaultItemAnimator()
            layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL)
            adapter = mForecastAdapter

        }
        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            showDialog()
        }
    }

    private fun initObservers() {
        mWeatherViewModel.weatherData.observe(
            this,
            Observer(function = fun(weatherData: WeatherResponse?) {
                weatherData?.let {
                    updateUI(it)
                }
            })
        )
        mWeatherViewModel.errorMessage.observe(this, {
            if (it != null) {
                showErrorDialog(it);
            }
        })
    }

    private fun updateUI(weatherResponse: WeatherResponse) {
        supportActionBar?.setTitle(weatherResponse.timezone)
        val current = weatherResponse.current
        val weather = current!!.weather.first()
        mCurrentTemp.text = String.format(Locale.getDefault(), "%.0f°", current.temp)
        mCurrentWeatherDescription.text = weather.description
        mCurrentHumidity.text = String.format(Locale.getDefault(), "%s%%", current.humidity)
        mCurrentWind.text = String.format(Locale.getDefault(), "%skm/h", current.wind_speed)
        mCurrentSunrise.text =
            String.format(Locale.getDefault(), "%shs", getHourAndMinute(current.sunrise))
        mCurrentSunset.text =
            String.format(Locale.getDefault(), "%shs", getHourAndMinute(current.sunset))
        Glide.with(this).load(weather.icon?.let { Constants.getUrlIcon(it) })
            .into(findViewById(R.id.current_weather_icon))
        mForecastAdapter.apply {
            addForecasts(weatherResponse.daily)
            notifyDataSetChanged()
        }

        mTodayMaterialCard.visibility = VISIBLE
        mRecycler.visibility = VISIBLE
        mProgress.visibility = GONE
    }

    private fun getHourAndMinute(dt: Long): String {
        return SimpleDateFormat("HH:mm", Locale.getDefault()).format(dt * 1000)
    }

    private fun initLocationProviderClient() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            mFusedLocationClient!!.lastLocation.addOnSuccessListener(this) { location: Location? ->
                if (location != null) {
                    mCurrentLocation = location
                    getWeatherData(
                        mCurrentLocation!!.latitude,
                        mCurrentLocation!!.longitude
                    )
                }
            }
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this@MainActivity,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            ) {
                ActivityCompat.requestPermissions(
                    this@MainActivity, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), 1
                )
            } else {
                ActivityCompat.requestPermissions(
                    this@MainActivity, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), 1
                )
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        initLocationProviderClient()
                    }
                } else {
                    Toast.makeText(this@MainActivity, "We can't find your location, please approve location permissions", Toast.LENGTH_SHORT).show()
                    locationSelected(1)
                }
                return
            }
        }
    }

    private fun showDialog() {
        val alertDialogBuilder = AlertDialog.Builder(this)
        with(alertDialogBuilder)
        {
            setTitle("List of Locations")
            setItems(locations) { dialog, which ->
                locationSelected(which)
            }
            show()
        }
    }

    private fun showErrorDialog(msg: String) {
        val alertDialogBuilder = AlertDialog.Builder(this)
        with(alertDialogBuilder)
        {
            setTitle("Error")
            setMessage(msg)
            setCancelable(false)
            setPositiveButton("Reload", DialogInterface.OnClickListener { dialogInterface, i ->
                locationSelected(locationSelected)
            })
            show()
        }
    }

    private fun locationSelected(which: Int) {
        locationSelected = which
        when (which) {
            0 -> {
                if (mCurrentLocation != null) {
                    getWeatherData(
                        mCurrentLocation!!.latitude,
                        mCurrentLocation!!.longitude
                    )
                } else {
                    Toast.makeText(this@MainActivity, "We can't find your location", Toast.LENGTH_SHORT).show()
                }
            }
            1 -> {
                getWeatherData(40.4165, -3.70256)
            }
            2 -> {
                getWeatherData(40.730610, -73.935242)
            }
            3 -> {
                getWeatherData(52.520008, 13.404954)
            }
            4 -> {
                getWeatherData(50.073658, 14.418540)
            }
            5 -> {
                getWeatherData(55.751244, 37.618423)
            }
        }

    }

    private fun getWeatherData(lat: Double, lng: Double) {
        mTodayMaterialCard.visibility = GONE
        mRecycler.visibility = GONE
        mProgress.visibility = VISIBLE
        mWeatherViewModel.getWeatherData(lat, lng)
    }
}