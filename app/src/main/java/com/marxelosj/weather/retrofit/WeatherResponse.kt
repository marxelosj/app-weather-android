package com.marxelosj.weather.retrofit

import com.google.gson.annotations.SerializedName

data class WeatherResponse(
    @SerializedName("lon")
    var lon: Double = 0.toDouble(),

    @SerializedName("lat")
    var lat: Double = 0.toDouble(),

    @SerializedName("timezone")
    var timezone: String? = null,

    @SerializedName("timezone_offset")
    var timezone_offset: Long = 0,

    @SerializedName("current")
    var current: Current? = null,

    @SerializedName("daily")
    var daily: ArrayList<Daily> = ArrayList()
)

data class Current(
    @SerializedName("dt")
    var dt: Long = 0,

    @SerializedName("sunrise")
    var sunrise: Long = 0,

    @SerializedName("sunset")
    var sunset: Long = 0,

    @SerializedName("temp")
    var temp: Float = 0F,

    @SerializedName("feels_like")
    var feels_like: Float = 0F,

    @SerializedName("pressure")
    var pressure: Float = 0F,

    @SerializedName("humidity")
    var humidity: Float = 0F,

    @SerializedName("dew_point")
    var dew_point: Float = 0F,

    @SerializedName("uvi")
    var uvi: Float = 0F,

    @SerializedName("clouds")
    var clouds: Float = 0F,

    @SerializedName("visibility")
    var visibility: Float = 0F,

    @SerializedName("wind_speed")
    var wind_speed: Float = 0F,

    @SerializedName("wind_deg")
    var wind_deg: Float = 0F,

    @SerializedName("weather")
    var weather: ArrayList<Weather> = ArrayList()
)

data class Daily(
    @SerializedName("dt")
    var dt: Long = 0,

    @SerializedName("sunrise")
    var sunrise: Long = 0,

    @SerializedName("sunset")
    var sunset: Long = 0,

    @SerializedName("pressure")
    var pressure: Float = 0F,

    @SerializedName("humidity")
    var humidity: Float = 0F,

    @SerializedName("dew_point")
    var dew_point: Float = 0F,

    @SerializedName("uvi")
    var uvi: Float = 0F,

    @SerializedName("clouds")
    var clouds: Float = 0F,

    @SerializedName("pop")
    var pop: Float = 0F,

    @SerializedName("wind_speed")
    var wind_speed: Float = 0F,

    @SerializedName("wind_deg")
    var wind_deg: Float = 0F,

    @SerializedName("weather")
    var weather: ArrayList<Weather> = ArrayList(),

    @SerializedName("temp")
    var temp: Temp? = null,

    @SerializedName("feels_like")
    var feels_like: FeelsLike? = null
)

data class Weather(
    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("main")
    var main: String? = null,

    @SerializedName("description")
    var description: String? = null,

    @SerializedName("icon")
    var icon: String? = null
)

data class Temp(
    @SerializedName("day")
    var day: Float = 0F,

    @SerializedName("min")
    var min: Float = 0F,

    @SerializedName("max")
    var max: Float = 0F,

    @SerializedName("night")
    var night: Float = 0F,

    @SerializedName("eve")
    var eve: Float = 0F,

    @SerializedName("morn")
    var morn: Float = 0F
)

data class FeelsLike(
    @SerializedName("day")
    var day: Float = 0F,

    @SerializedName("night")
    var night: Float = 0F,

    @SerializedName("eve")
    var eve: Float = 0F,

    @SerializedName("morn")
    var morn: Float = 0F,
)
