package com.marxelosj.weather.retrofit

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {
    @GET("data/2.5/onecall?")
    fun getOneCallWeatherData(
        @Query("lat") lat: String,
        @Query("lon") lng: String,
        @Query("exclude") exclude: String,
        @Query("units") units: String,
        @Query("lang") lang: String,
        @Query("appid") appid: String
    ): Call<WeatherResponse>
}