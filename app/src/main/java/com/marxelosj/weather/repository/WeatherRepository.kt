package com.marxelosj.weather.repository

import com.marxelosj.weather.retrofit.WeatherApi
import com.marxelosj.weather.retrofit.WeatherResponse
import com.marxelosj.weather.utils.Constants
import retrofit2.awaitResponse
import java.lang.Exception
import java.util.*

class WeatherRepository(val weatherApi: WeatherApi) {

    suspend fun getWeatherData(lat: Double, lng: Double, onWeatherData: OnWeatherData) {
        try {
            val response = weatherApi.getOneCallWeatherData(
                lat.toString(),
                lng.toString(),
                Constants.EXCLUDE,
                Constants.UNITS,
                Locale.getDefault().language,
                Constants.APP_ID
            ).awaitResponse()
            if (response.isSuccessful) {
                onWeatherData.onSuccess((response.body() as WeatherResponse))
            } else {
                onWeatherData.onFailure("Error -> " + response.code())
            }
        } catch (e: Exception) {
            onWeatherData.onFailure(e.message.toString())
        }
    }

    interface OnWeatherData {
        fun onSuccess(data: WeatherResponse)
        fun onFailure(msg: String)
    }
}