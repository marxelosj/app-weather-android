package com.marxelosj.weather.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.marxelosj.weather.repository.WeatherRepository
import com.marxelosj.weather.retrofit.WeatherResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import java.lang.Exception

@KoinApiExtension
class WeatherViewModel(val dataRepository: WeatherRepository) : ViewModel(), KoinComponent {

    var weatherData = MutableLiveData<WeatherResponse>()
    val errorMessage = MutableLiveData<String>()

    init {
        weatherData.value = null
        errorMessage.value = null
    }

    fun getWeatherData(lat: Double, lng: Double) {
        GlobalScope.launch(Dispatchers.IO) {
            dataRepository.getWeatherData(lat, lng, object : WeatherRepository.OnWeatherData {
                override fun onSuccess(data: WeatherResponse) {
                    weatherData.postValue(data)
                    errorMessage.postValue(null)
                }

                override fun onFailure(msg: String) {
                    weatherData.postValue(null)
                    errorMessage.postValue(msg)
                }
            })
        }
    }

}