package com.marxelosj.weather

import android.app.Application
import com.marxelosj.weather.retrofit.weatherModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class WeatherApp : Application(){
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@WeatherApp)
            modules(weatherModule)
        }
    }
}