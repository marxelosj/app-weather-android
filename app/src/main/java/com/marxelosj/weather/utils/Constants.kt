package com.marxelosj.weather.utils

object Constants {
    const val BASE_URL = "http://api.openweathermap.org/"
    const val UNITS = "metric"
    const val EXCLUDE = "minutely,hourly,alerts"
    const val APP_ID = "2e65127e909e178d0af311a81f39948c"
    fun getUrlIcon(icon: String): String {
        return StringBuilder().append("http://openweathermap.org/img/wn/").append(icon)
            .append("@2x.png").toString()
    }
}